package cl.ubb.test;

import static org.junit.Assert.*;

import org.junit.Test;

public class PerfectoTest {

	@Test
	public void testPerfecto6() {
		NumeroPerfecto perfecto= new NumeroPerfecto();
		boolean resultado;
		resultado=perfecto.EsPerfecto(6);
		assertEquals(true,resultado);
	}
	@Test
	public void testPerfecto28(){
		NumeroPerfecto perfecto= new NumeroPerfecto();
		boolean resultado;
		resultado=perfecto.EsPerfecto(28);
		assertEquals(true,resultado);
	}
	@Test 
	public void testPerfecto496(){
		NumeroPerfecto perfecto = new NumeroPerfecto();
		boolean resultado;
		resultado=perfecto.EsPerfecto(496);
		assertEquals(true,resultado);
	}

}
